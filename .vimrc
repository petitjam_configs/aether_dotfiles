syntax on
filetype plugin indent on

set number
set expandtab
set tabstop=2
set shiftwidth=2

" Copied from old computer, no idea what it is
nmap <CR> o<ESC>

au BufReadPost Screengrabfile set syntax=ruby
au BufReadPost .gitconfig* set syntax=gitconfig
