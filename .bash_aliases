# General

alias resrc="source ~/.bashrc"

alias g='git '
[ -f /usr/share/bash-completion/completions/git ] && . /usr/share/bash-completion/completions/git
__git_complete g __git_main

alias gronk='ngrok http -subdomain=amp 3000'

# Rails

alias raub='rails app:update:bin'
alias rdms='rails db:migrate:status'
alias rdtp='rails db:test:prepare'

alias bi='bundle install'
alias be='bundle exec '

# Misc

alias xclip="xclip -selection c"
alias 🐈='echo MEOW'
alias mdvpn="sudo openvpn --config $HOME/doorman.ovpn"
alias gs="echo That\'s not git!"

# Functions

rdr() {
	rake db:restore FILE=$1
}

turnoffcapslock() {
  python -c 'from ctypes import *; X11 = cdll.LoadLibrary("libX11.so.6"); display = X11.XOpenDisplay(None); X11.XkbLockModifiers(display, c_uint(0x0100), c_uint(2), c_uint(0)); X11.XCloseDisplay(display)'
}
alias TURNOFFCAPSLOCK='turnoffcapslock'
